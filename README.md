# CSS grid playground
This repository includes a responsive css grid layout. **Note** that css grids are **not** fully supported by Internet Explore. Check [here](https://caniuse.com/#search=grid) for the actual support.

## Build project

`$ yarn`

`$ gulp watch`



## Grid

##### Usage:

```html
<div class="row">
	<div class="col--12 col-sm--6"></div>
	<div class="col--12 col-sm--6"></div>
</div>
```

or 

```html
<div class="row">
	<div class="col--2-5 col-sm--2-8"></div>
</div>
```

`col--2-5` will gives you a column which start at column 2 and ends at column 5.





****

##### Have fun!