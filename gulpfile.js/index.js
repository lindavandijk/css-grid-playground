/**
 * @file In this file are the main gulp tasks,
 * to execute them, run gulp build or gulp watch.
 * build will build the application (use --production for minified css & js),
 * watch will build & watch for filechanges.
 */
var gulp = require('gulp'),
    GlobalConfig = require('./global-config'),
    requireDir = require('require-dir'),
    tasks = requireDir('./tasks', { recurse: true, camelcase: true });

gulp.task('default', ['build']);

gulp.task('build', [
    tasks.browserify.build,
    tasks.sass.build,
    tasks.handlebars.build,
    tasks.images.build
]);

gulp.task('watch', [
    tasks.lint.watch,
    tasks.browserify.watch,
    tasks.sass.watch,
    tasks.handlebars.watch,
    tasks.images.watch,
    'init-browser-sync'
]);

gulp.task('init-browser-sync', [tasks.browserify.watch], function () {
    var stylesheet = (GlobalConfig.environment === 'production') ? 'style.min.css' : 'style.css';

    tasks.browserSync.init({
        files: [
            GlobalConfig.destPath + '/assets/css/' + stylesheet,
            GlobalConfig.destPath + '/assets/images/**/*.{jpg,jpeg,svg,gif,png}'
        ],
        server: GlobalConfig.destPath
    });
});