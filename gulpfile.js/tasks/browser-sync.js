/**
 * @file Export a server so we can use it in other tasks
 */
var browserSync = require('browser-sync').create();
module.exports = browserSync;