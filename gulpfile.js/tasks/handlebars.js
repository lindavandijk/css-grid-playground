/**
 * @file This task compiles the handlebars templates to html,
 * When --production flag is used, the paths to css & js will be replaced
 * */
var gulp = require('gulp'),
    GlobalConfig = require('./../global-config'),
    handlebars = require('gulp-compile-handlebars'),
    htmlreplace = require('gulp-html-replace'),
    rename = require('gulp-rename'),
    browserSync = require('./browser-sync'),
    gulpif = require('gulp-if');

/**
 * Local config that's overwritten by properties from the global config if they exist
 * @type {Object}
 */
var config = Object.assign({
    all: GlobalConfig.srcPath + '/templates/**/*.hbs',
    dir: GlobalConfig.srcPath + '/templates',
    src: GlobalConfig.srcPath + '/templates/*.hbs',
    dest: GlobalConfig.destPath
}, GlobalConfig.handlebars);

gulp.task('handlebars', function () {
    var siteData = {};

    var options = {
        ignorePartials: false,
        batch: [config.dir]
    };

    var isDev = GlobalConfig.environment == 'development';

    return gulp.src(config.src)
        .pipe(handlebars(siteData, options))
        .pipe(rename(function (path) {
            path.extname = '.html'
        }))
        .pipe(gulpif( !isDev, htmlreplace({
            'css': 'assets/css/style.min.css',
            'js': 'assets/js/main.min.js'
        })))
        .pipe(gulp.dest(config.dest));
});

gulp.task('handlebars-watch', ['handlebars'], function () {
    return gulp.watch(config.all, ['handlebars', browserSync.reload]);
});

/**
 * @module tasks/handlebars
 * @type {{build: string, watch: string}}
 */
module.exports = {
    build: 'handlebars',
    watch: 'handlebars-watch'
};