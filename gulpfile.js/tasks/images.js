/**
 * @file This task optimizes the images and copies them to the dist folder
 */
var gulp = require('gulp'),
    GlobalConfig = require('./../global-config'),
    changed = require('gulp-changed'),
    imagemin = require('gulp-imagemin');

/**
 * Local config that's overwritten by properties from the global config if they exist
 * @type {Object}
 */
var config = Object.assign({
    src: GlobalConfig.srcPath + '/assets/images/**/*.{jpg,jpeg,svg,gif,png}',
    dest: GlobalConfig.destPath + '/assets/images/'
}, GlobalConfig.images);

gulp.task('images', function () {
    return gulp.src(config.src)
        .pipe(changed(config.dest))
        .pipe(imagemin({
            progressive: true,
            optimizationLevel: 5
        }))
        .pipe(gulp.dest(config.dest));
});

gulp.task('images-watch', ['images'], function () {
    return gulp.watch(config.src, ['images']);
});

/**
 * @module tasks/images
 * @type {{build: string, watch: string}}
 */
module.exports = {
    build: 'images',
    watch: 'images-watch'
};