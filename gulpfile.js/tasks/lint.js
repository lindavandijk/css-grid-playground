/**
 * @file Task to lint the javascript, also uses jscs to check the javascript styling
 */
var gulp = require('gulp'),
    GlobalConfig = require('./../global-config'),
    jshint = require('gulp-jshint'),
    jscs = require('gulp-jscs'),
    jscsStylish = require('gulp-jscs-stylish');

/**
 * Local config that's overwritten by properties from the global config if they exist
 * @type {Object}
 */
var config = Object.assign({
    src: [ GlobalConfig.srcPath + '/assets/js/**/*.js']
}, GlobalConfig.lint);

gulp.task('lint', function () {
    return gulp.src(config.src)
        .pipe(jshint('.jshintrc'))
        .pipe(jscs())
        .pipe(jscsStylish.combineWithHintResults())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('lint-watch', ['lint'], function () {
    gulp.watch(config.src, ['lint']);
});

/**
 * @module tasks/lint
 * @type {{build: string, watch: string}}
 */
module.exports = {
    build: 'lint',
    watch: 'lint-watch'
};