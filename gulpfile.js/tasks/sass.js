/**
 * @file This tasks compiles scss into css,
 * adds sourcemaps,
 * uses autoprefixer for vendor prefixes,
 * minifies the css file when using --production.
 */
var gulp = require('gulp'),
    GlobalConfig = require('./../global-config'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    nano = require('gulp-cssnano'),
    gulpif = require('gulp-if');

/**
 * Local config that's overwritten by properties from the global config if they exist
 * @type {Object}
 */
var config = Object.assign({
    all: GlobalConfig.srcPath + '/assets/sass/**/*.scss',
    src: GlobalConfig.srcPath + '/assets/sass/main.scss',
    dest: GlobalConfig.destPath + '/assets/css/',
    filename: 'style',
    autoprefixer: ['last 2 versions']
}, GlobalConfig.sass);

gulp.task('sass', ['iconfont'], function () {
    var isDev = GlobalConfig.environment == 'development';

    return gulp.src(config.src)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: config.autoprefixer
        }))
        .pipe(rename({
            basename: config.filename
        }))
        .pipe(gulpif(!isDev, rename({
            suffix: '.min'
        })))
        .pipe(gulpif( !isDev, nano()))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.dest));
});

gulp.task('sass-watch', ['sass'], function () {
    return gulp.watch(config.all, ['sass']);
});

/**
 * @module tasks/sass
 * @type {{build: string, watch: string}}
 */
module.exports = {
    build: 'sass',
    watch: 'sass-watch'
};